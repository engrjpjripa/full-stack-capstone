import React from 'react';
import {Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Product(props){
	// Props is used here to get the data and breakPoint from the FeaturedCourses.js
	const {breakPoint, data} = props

	const {_id, name, description, price} = data

	return(

		<Col xs={12} md={4} lg={3}>
			{/*Adding the class cardHighlight for min-height*/}
			<Card className="cardHighlight mx-2">
				<Card.Body>
					<>
					    {
					        // HP 14S-CF2041TU
					        (_id === '650158eba72047e8c04e6a55') ?
					            <img id="productImage" src="https://s3.pricemestatic.com/Large/Images/RetailerProductImages/StRetailer1338/rp_39105966_0027393716_l.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
					            :
					        // ASUS X512DK-EJ238T
					        (_id === '65015974a72047e8c04e6a57') ?
					            <img id="productImage" src="https://bermorzone.com.ph/wp-content/uploads/2020/01/500x_17f7fae3-7de9-487e-80d9-16f1acf40db1_grande.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
					            :
					        // LENOVO S145-14 (81UV004MPH)
					        (_id === '650159e6a72047e8c04e6a59') ?
					            <img id="productImage" src="https://www.octagon.com.ph/cdn/shop/products/L3_6d4b2e04-c803-40bb-86cb-ec6e9468e15c_2048x2048.jpg?v=1598413631" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
					            :
					        // HP PAVILION DT TP01-0110D
					        (_id === '6511596f9c412b6b173f735f') ?
					            <img id="productImage" src="https://www.compex.com.ph/cdn/shop/products/HPPavTP01-0110dDTPC_1200x1200.jpg?v=1592277805" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
					            :
					        // ACER NITRO 5 AN515-58-55LG
					        (_id === '6511641f56661a86d84db8d5') ?
					            <img id="productImage" src="https://s3.pricemestatic.com/Large/Images/RetailerProductImages/StRetailer35/rp_36018111_0092304313_l.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>    
					            :
					        // ACER ASPIRE TC1750-I78G
					        (_id === '65129e37b575603258ee4c11') ?
					            <img id="productImage" src="https://otcer.ph/wp-content/uploads/2023/06/Acer-Aspire-TC-Series.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>  
					            :
					        // ASUS TUF FX507ZM-HN073W
					        (_id === '65137933621bf855e638cd35') ?
					            <img id="productImage" src="https://cdn.shopify.com/s/files/1/0217/5985/2608/products/000001_28362_bebf602f-9cfa-4ddc-ab38-edfb20fe3026.jpg?v=1660201807" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
					            :
					        // ASUS X509FJ-FT871T
					        (_id === '651379a7621bf855e638cd39') ?
					            <img id="productImage" src="https://www.octagon.com.ph/cdn/shop/products/asus1_bb50bacb-0cf7-46bc-ac18-6c7878b1ab8a_2048x2048.jpg?v=1598323125" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
					            :    
					            <></>
					    }
					</>
					<Card.Title className="text-center">
					<Link to={`/products/${_id}`}>{name}</Link>
					</Card.Title>
				</Card.Body>
				<Card.Footer>
					<h5 className="text-danger text-center">₱{price}</h5>
					<Link className="btn btn-light d-block" to={`/products/${_id}`}>&#128065;View</Link>
				</Card.Footer>
			</Card>
		</Col>
	)
}