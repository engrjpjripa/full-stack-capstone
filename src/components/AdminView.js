import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import { Link } from 'react-router-dom';

export default function AdminView({ productsData, fetchData }) {

    // b. Add state to store all products 
    const [products, setProducts] = useState([])


    //Getting the productsData from the products page
    useEffect(() => {
        const productsArr = productsData.map(product => {
            return (
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td className="text-primary">{product.name}</td>
                    <td>{product.description}</td>
                    <td>&#x20B1;{product.price}</td>
                    <td className={product.isActive ? "text-success" : "text-danger"}>
                        {product.isActive ? "Available" : "Out of Stock"}
                    </td>
                    <td><EditProduct product={product._id} fetchData={fetchData}/></td> 
                    <td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} /></td>    
                </tr>
                )
        })

        setProducts(productsArr)

    }, [productsData])


    return(
        <>
            <h1 className="text-center my-4"> Admin Dashboard</h1>
            <Link className="btn btn-success" to={'/addProduct'}>Add Product</Link>
            
            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th className="bg-dark text-light">ID</th>
                        <th className="bg-dark text-light">Name</th>
                        <th className="bg-dark text-light">Description</th>
                        <th className="bg-dark text-light">Price</th>
                        <th className="bg-dark text-light">Availability</th>
                        <th className="bg-dark text-light" colSpan="2">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {products}
                </tbody>
            </Table>    
        </>

        )
}


