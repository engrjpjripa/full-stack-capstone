import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import {useState, useContext} from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){
	// // State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("token"));
	// console.log(user);
	const {user} = useContext(UserContext);

	return(
		//<Navbar bg="light" expand="lg">
		<Navbar className="navbar navbar-dark bg-black" expand="lg">
	        <Container>
	            <Navbar.Brand className="brand" as={Link} to="/">パソコン</Navbar.Brand>
	            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
	            <Navbar.Collapse id="basic-navbar-nav">
	                <Nav className="ms-auto tex">
	                <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
		                {(user.id !== null) ?       
	                        <Nav.Link as={Link} to="/logout">Logout</Nav.Link>       
	                        : 
                            <>
                               <Nav.Link as={Link} to="/login">Login</Nav.Link>
                               <Nav.Link as={Link} to="/register">Register</Nav.Link>
                            </>
	                    }
	                </Nav>
	            </Navbar.Collapse>
	        </Container>
	    </Navbar>
	)
}