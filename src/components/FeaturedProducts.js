import {CardGroup} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState,useEffect} from 'react';
import PreviewProducts from './PreviewProducts';

export default function FeaturedProducts(){
	
	const [previews, setPreviews] = useState([])

	useEffect(() => {
		fetch(`https://capstone2-jjyh.onrender.com/b21/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			// create two empty arrays to be used to store random numbers and featured products data
			const numbers = []
			const featured = []

			// this function generates a random number between 0 and the length of the data array(fetched product data). it checks if the random number has already been added to the numbers array. if not, it adds the random number to the numbers array. if the random number already exists in the numbers array, it recursively call itself to generate a new number
			const generateRandomNums = () => {
				let randomNum = Math.floor(Math.random() * data.length)
				if(numbers.indexOf(randomNum) === -1){
					numbers.push(randomNum)
				}else{
					generateRandomNums()
				}
			}

			// a loop is used to iterate four times (from 0 to 4). inside the loop, the generatedRandomNums function is called to generate a random number and push it into the numbers array
			for(let i = 0; i < 4; i++){
				generateRandomNums()

				// for each iteration of the loop, the PreviewProducts component is rendered with the corresponding product data from the data array based on the random number. the key prop set to the _id of the product for React's reconciliation to track components
				featured.push(
					<PreviewProducts data={data[numbers[i]]} key={data[numbers[i]]._id} breakPoint={2}/>
				)
			}

			// after the loop finishes, the setPreviews function is called to update the state of the component with the featured array. This state update triggers a re-render, and the PreviewProducts components are displayed on the page
			setPreviews(featured)
		})
	}, [])

	return(
		<>
			<h2 className="text-center">Featured Products</h2>
			<CardGroup className="justify-content-center">
				{previews}
			</CardGroup>
		</>
	)
}