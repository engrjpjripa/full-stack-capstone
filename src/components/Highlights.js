import {Container, Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Container fluid>
            <Row className="pt-3 pb-3">
                <Col xs={12} md={4}>
                    <Card className="cardHighlight p-3">
                        <Card.Body>
                            <Card.Title>
                                <h2>Customer Satisfaction</h2>
                            </Card.Title>
                            <Card.Text className="text-secondary">
                                We pride ourselves on providing high-quality products, from all the leading technology brands, and excellent customer service. We have a team of experienced and knowledgeable staff who are able to assist customers with their purchases, and provide technical support if required.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="cardHighlight p-3">
                        <Card.Body>
                            <Card.Title>
                                <h2>Nationwide Delivery</h2>
                            </Card.Title>
                            <Card.Text className="text-secondary">
                                We deliver anywhere in the Philippines. For speedy and reliable delivery we are using the services of LBC Express, the largest Express Courier and Cargo service company in the Philippines.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="cardHighlight p-3">
                        <Card.Body>
                            <Card.Title>
                                <h2>Returns and Warranty Policy</h2>
                            </Card.Title>
                            <Card.Text className="text-secondary">
                                We understand that equipment failure can be a frustrating experience which is why we always aim to resolve these issues in the shortest time possible.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
	)
}