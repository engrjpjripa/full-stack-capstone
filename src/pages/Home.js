import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';
import UserContext from '../UserContext';
import {useEffect, useState, useContext} from 'react';

export default function Home(){

    const { user } = useContext(UserContext);

    const data = {
        title: "パソコン - Philippines' best computer store!",
        content: "(Pronounced as Pasokon, the Japanese word for Personal Computer, was established in 2023. We are Manila's best computer shop, dedicated to serving business customers from across Philippines. We have unparalleled range of the latest laptops, desktops, sofware, computer parts and everything else in IT. We also offer affordable computer and laptop repairs.",
        destination: "/products",
        label: "Order now!"
    }

    return(
        <>
            <Banner data={data} />
                {(user.isAdmin === true) ?       
                    <></>     
                    : 
                    <>
                       <FeaturedProducts />
                    </>
                }
            
            <Highlights />           
        </>
    )
}
