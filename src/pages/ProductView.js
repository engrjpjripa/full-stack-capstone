import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve any parameter or the productId passed via the URL
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	// Allows us to gain access to methods that will allow us to redirect a user to a different page after ordering a product

	//an object with methods to redirect the user
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [id, setId] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const myArray = [
		{productId: id, quantity: 1}
	];
	console.log(myArray);

	const order = (productId) => {

		// Checkout Order
		fetch(`https://capstone2-jjyh.onrender.com/b21/orders/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				products: myArray,
				totalAmount: price*1
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data.message);

			if (data.message === 'Order received!') {

				Swal.fire({
					title: "Order Succesful",
					icon: 'success',
					text: "You have successfully ordered this product."
				});

				// The "navigate" method allows us to redirect the user to a different page and is an easier approach rather than using the "Navigate" component
				navigate("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		});

	};

	useEffect(()=> {

		console.log(productId);

		// Retrieve Single Product
		fetch(`https://capstone2-jjyh.onrender.com/b21/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setId(data._id);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<>
						    {
						        // HP 14S-CF2041TU
						        (id === '650158eba72047e8c04e6a55') ?
						            <img id="productImage" src="https://s3.pricemestatic.com/Large/Images/RetailerProductImages/StRetailer1338/rp_39105966_0027393716_l.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
						            :
						        // ASUS X512DK-EJ238T
						        (id === '65015974a72047e8c04e6a57') ?
						            <img id="productImage" src="https://bermorzone.com.ph/wp-content/uploads/2020/01/500x_17f7fae3-7de9-487e-80d9-16f1acf40db1_grande.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
						            :
						        // LENOVO S145-14 (81UV004MPH)
						        (id === '650159e6a72047e8c04e6a59') ?
						            <img id="productImage" src="https://www.octagon.com.ph/cdn/shop/products/L3_6d4b2e04-c803-40bb-86cb-ec6e9468e15c_2048x2048.jpg?v=1598413631" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
						            :
						        // HP PAVILION DT TP01-0110D
						        (id === '6511596f9c412b6b173f735f') ?
						            <img id="productImage" src="https://www.compex.com.ph/cdn/shop/products/HPPavTP01-0110dDTPC_1200x1200.jpg?v=1592277805" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
						            :
						        // ACER NITRO 5 AN515-58-55LG
						        (id === '6511641f56661a86d84db8d5') ?
						            <img id="productImage" src="https://s3.pricemestatic.com/Large/Images/RetailerProductImages/StRetailer35/rp_36018111_0092304313_l.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>    
						            :
						        // ACER ASPIRE TC1750-I78G
						        (id === '65129e37b575603258ee4c11') ?
						            <img id="productImage" src="https://otcer.ph/wp-content/uploads/2023/06/Acer-Aspire-TC-Series.jpg" className="card-img-top rounded mx-auto d-block"  alt="..."></img>  
						            :
						        // ASUS TUF FX507ZM-HN073W
						        (id === '65137933621bf855e638cd35') ?
						            <img id="productImage" src="https://cdn.shopify.com/s/files/1/0217/5985/2608/products/000001_28362_bebf602f-9cfa-4ddc-ab38-edfb20fe3026.jpg?v=1660201807" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
						            :
						        // ASUS X509FJ-FT871T
						        (id === '651379a7621bf855e638cd39') ?
						            <img id="productImage" src="https://www.octagon.com.ph/cdn/shop/products/asus1_bb50bacb-0cf7-46bc-ac18-6c7878b1ab8a_2048x2048.jpg?v=1598323125" className="card-img-top rounded mx-auto d-block"  alt="..."></img>
						            :    
						            <></>
						    }
						</>
						<Card.Body className="text-center">
							<Card.Title className="text-primary">{name}</Card.Title>
							<Card.Subtitle>Specification:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							{/*<Card.Subtitle>Price:</Card.Subtitle>*/}
							<Card.Text className="text-danger">&#x20B1;{price}</Card.Text>
							{ user.id !== null ? 
								<Button variant="danger" block onClick={() => order(productId)}>Checkout</Button>
								: 
								<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
