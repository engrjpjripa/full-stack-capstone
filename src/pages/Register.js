import {Container, Form, Button, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { useNavigate, Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){
	
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if the values are logged
	console.log(email);
	console.log(password);
	console.log(confirmPassword);

	
	// Register User
	function registerUser(e){
		// Prevents page redirection via form submissions
		e.preventDefault();

		fetch(`https://capstone2-jjyh.onrender.com/b21/users/register`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// data will only contain an email property if we can properly save our user
			if(data){
				// Clear input fields
				setEmail("");
				setPassword("");
				setConfirmPassword("")

				Swal.fire({
				    title: "Registration Successful",
				    icon: "success"
				})

				navigate("/login");
			} else {
				Swal.fire({
				    title: "Registration Failed",
				    icon: "error",
				    text: "Check your details and try again"
				})
			}	
		})

	}

	useEffect(()=>{
		if(email !== "" && password !=="" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email, password, confirmPassword])


	return(
		(user.id !== null) ?
			<Navigate to="/login" />
		:
			<Container>	
				<Row>
					<Col lg={{ span: 6, offset: 3 }}>
						<Form onSubmit={(e) => registerUser(e)}>
					        <h1 className="my-5 text-center">Sign Up</h1>
					            <Form.Group className="mt-3">
					                <Form.Label>Email:</Form.Label>
					                <Form.Control type="email" placeholder="Enter your email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
					            </Form.Group>
					            <Form.Group className="mt-3">
					                <Form.Label>Password:</Form.Label>
					                <Form.Control type="password" placeholder="Enter your password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
					            </Form.Group>
					            <Form.Group className="my-3">
					                <Form.Label>Confirm Password:</Form.Label>
					                <Form.Control type="password" placeholder="Verify your password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}/>
					            </Form.Group>
					            { 
					            	isActive
									?<Button variant="warning" type="submit" id="submitBtn" to="/login">Submit</Button>
					            	:<Button variant="warning" type="submit" id="submitBtn" disabled>Please enter your registration details</Button>
					            }      
				        </Form>
				        <p className="text-center mt-3">
			                <span> Already have an account? </span>
			                <Link className="text-decoration-none" to="/login">Click here</Link>
			                <span> to log in.</span>
			            </p>
			        </Col>
		        </Row>
	        </Container>
	)
}