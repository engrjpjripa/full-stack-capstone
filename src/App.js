import './App.css';
import {UserProvider} from './UserContext';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AddProduct from './pages/AddProduct';
import ProductView from './pages/ProductView';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';


function App() {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    fetch(`https://capstone2-jjyh.onrender.com/b21/user/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(typeof data._id !== "undefined"){
        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        });
      // Else set the user state to the initial values
      }else{
          setUser({
            id: null,
            isAdmin: null
          })
      }
    })
  }, [])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
            <AppNavbar />
              <Routes>
                  <Route path="/" element={<Home/>} />
                  <Route path="/products" element={<Products/>} />
                  <Route path="/products/:productId" element={<ProductView/>} />
                  <Route path="/register" element={<Register/>} />
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="/addProduct" element={<AddProduct/>}/>
                  <Route path="*" element={<Error/>}/>
              </Routes>
        </Container>
      </Router>
    </UserProvider>   
  );
}

export default App;
